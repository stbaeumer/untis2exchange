﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Untis2Exchange
{
    static class Tagesbericht
    {
        static List<string> list; // Static List instance
        static List<Tuple<Lehrer, DateTime>> lehrersOhneUnterricht;
        static List<Tuple<Abwesenheit, DateTime>> abwesendeLehrer;

        public static DateTime DatumTagesbericht { get; set; }

        static Tagesbericht()
        {            
            list = new List<string>();
            lehrersOhneUnterricht = new List<Tuple<Lehrer, DateTime>>();
            abwesendeLehrer = new List<Tuple<Abwesenheit, DateTime>>();
        }

        internal static void AddLehrerOhneUnterrichtUndOhnePausenaufsicht(Lehrer lehrer, DateTime datumTagDerKalenderwoche)
        {
            DatumTagesbericht = datumTagDerKalenderwoche;
            lehrersOhneUnterricht.Add(new Tuple<Lehrer, DateTime>(lehrer, datumTagDerKalenderwoche));
        }

        internal static void AddAbwesende(Abwesenheit abwesenheit, DateTime datumTagDerKalenderwoche)
        {
            DatumTagesbericht = datumTagDerKalenderwoche;
            abwesendeLehrer.Add(new Tuple<Abwesenheit, DateTime>(abwesenheit, datumTagDerKalenderwoche));
        }

        public static void Record(string value)
        {
            list.Add(value);
        }

        public static void Display()
        {
            foreach (var value in list)
            {
                Console.WriteLine(value);
            }
        }

        internal static void Senden()
        {

            string ausgabe = "<H1>Tagesbericht " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + "</H1>";
                        
            ausgabe += "<H2>Kolleginnen und Kollegen ohne Unterricht und ohne Pausenaufsicht</H2>";
            
            ausgabe += "<OL>";
            foreach (var item in lehrersOhneUnterricht.OrderBy(x => x.Item2).ToList())
            {
                 ausgabe += "<li>" + ((Lehrer)(item.Item1)).Kürzel;                 
            }
            ausgabe += "</OL>";
                        
            ausgabe += "<H2>Abwesende Lehrer</H2>";
            ausgabe += "<OL>";
            foreach (var item in abwesendeLehrer.OrderBy(x => x.Item2).ToList())
            {
                ausgabe += "<li>" + ((Abwesenheit)(item.Item1)).LehrerKürzel.PadRight(3) + " " + ((Abwesenheit)(item.Item1)).VonDatum.ToShortDateString() + " - " + ((Abwesenheit)(item.Item1)).BisDatum.ToShortDateString() + " " + ((Abwesenheit)(item.Item1)).VonStunde.ToString().PadLeft(3) + ". - " + ((Abwesenheit)(item.Item1)).BisStunde.ToString().PadLeft(3) + ". Stunde   Grund:" + ((Abwesenheit)(item.Item1)).Langname;
            }
            ausgabe += "</OL>";

            Global.MailSenden("Tagesbericht " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString(), ausgabe);
        }
    }
}
