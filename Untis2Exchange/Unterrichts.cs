﻿
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace Untis2Exchange
{
    public class Unterrichts : List<Unterricht>
    {
        public Unterrichts()
        {
        }

        public Unterrichts(string aktSj, DateTime datumErsterTagDesPrüfZyklus, string connectionString, int periode, Klasses klasses, Lehrers lehrers, Fachs fachs, Raums raums, Unterrichtsgruppes unterrichtsgruppes)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                int id = 0;

                try
                {
                    string queryString = @"SELECT DISTINCT 
Lesson_ID,
LessonElement1,
Periods,
Lesson.LESSON_GROUP_ID,
Lesson_TT,
Flags,
DateFrom,
DateTo
FROM LESSON
WHERE (((SCHOOLYEAR_ID)= " + aktSj + ") AND ((TERM_ID)=" + periode + ") AND ((Lesson.SCHOOL_ID)=177659) AND (((Lesson.Deleted)=No))) ORDER BY LESSON_ID;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    Console.WriteLine("Unterrichte");
                    Console.WriteLine("-----------");

                    while (oleDbDataReader.Read())
                    {
                        id = oleDbDataReader.GetInt32(0);

                        if (id == 7696)
                        {
                            string a = "";
                        }
                        string wannUndWo = Global.SafeGetString(oleDbDataReader, 4);

                        var zur = wannUndWo.Replace("~~","|").Split('|');

                        ZeitUndOrts zeitUndOrts = new ZeitUndOrts();

                        for (int i = 0; i < zur.Length; i++)
                        {     
                            if(zur[i] != "")
                            {
                                var zurr = zur[i].Split('~');

                                int tag = 0;
                                int stunde = 0;
                                List<string> raum = new List<string>();

                                try
                                {
                                    tag = Convert.ToInt32(zurr[1]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Tag.");
                                }

                                try
                                {
                                    stunde = Convert.ToInt32(zurr[2]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keine Stunde.");
                                }

                                try
                                {
                                    var ra = zurr[3].Split(';');

                                    foreach (var item in ra)
                                    {
                                        if(item != "")
                                        {
                                            raum.AddRange((from r in raums
                                                           where item.Replace(";", "") == r.IdUntis.ToString()
                                                           select r.Raumnummer));
                                        }                                        
                                    }

                                    if (raum.Count == 0)
                                    {                                    
                                        raum.Add("");
                                    }
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Raum.");
                                }


                                ZeitUndOrt zeitUndOrt = new ZeitUndOrt(tag, stunde, raum);
                                zeitUndOrts.Add(zeitUndOrt);
                            }
                        }
                        
                        string lessonElement = Global.SafeGetString(oleDbDataReader, 1);
                        
                        int anzahlGekoppelterLehrer = lessonElement.Count(x => x == '~') / 21;

                        List<string> klassenKürzel = new List<string>();

                        for (int i = 0; i < anzahlGekoppelterLehrer; i++)
                        {
                            var lesson = lessonElement.Split(',');

                            var les = lesson[i].Split('~');
                            string lehrer = les[0] == "" ? null : (from l in lehrers where l.IdUntis.ToString() == les[0] select l.Kürzel).FirstOrDefault();
                            
                            string fach = les[2] == "0" ? "" : (from f in fachs where f.IdUntis.ToString() == les[2] select f.KürzelUntis).FirstOrDefault();

                            string raumDiesesUnterrichts = "";
                            if (les[3] != "")
                            {
                                raumDiesesUnterrichts = (from r in raums where (les[3].Split(';')).Contains(r.IdUntis.ToString()) select r.Raumnummer).FirstOrDefault();
                            }

                            int anzahlStunden = oleDbDataReader.GetInt32(2);
                                                        
                            var unterrichtsgruppeDiesesUnterrichts = (from u in unterrichtsgruppes where u.IdUntis == oleDbDataReader.GetInt32(3) select u).FirstOrDefault();
                            
                            if (les.Count() >= 17)
                            {                                
                                foreach (var kla in les[17].Split(';'))
                                {
                                    Klasse klasse = new Klasse();

                                    if (kla != "")
                                    {
                                        if (!(from kl in klassenKürzel
                                             where kl == (from k in klasses
                                                          where k.IdUntis == Convert.ToInt32(kla)
                                                          select k.NameUntis).FirstOrDefault()
                                             select kl).Any())
                                        {
                                            klassenKürzel.Add((from k in klasses
                                                               where k.IdUntis == Convert.ToInt32(kla)
                                                               select k.NameUntis).FirstOrDefault());
                                        }                                        
                                    }
                                }
                            }
                            else
                            {                                
                            }
                            
                            if (lehrer != null)
                            {
                                for (int z = 0; z < zeitUndOrts.Count; z++)
                                {
                                    // Wenn zwei Lehrer gekoppelt sind und zwei Räume zu dieser Stunde gehören, dann werden die Räume entsprechend verteilt.

                                    string r = zeitUndOrts[z].Raum[0];
                                    try
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[i];
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[0];
                                        }
                                    }

                                    string k = "";

                                    foreach (var item in klassenKürzel)
                                    {
                                        k += item + ",";
                                    }

                                    // Nur wenn der tagDesUnterrichts innerhalb der Befristung stattfindet, wird er angelegt

                                    DateTime von = DateTime.ParseExact((oleDbDataReader.GetInt32(6)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime bis = DateTime.ParseExact((oleDbDataReader.GetInt32(7)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                                    DateTime tagDesUnterrichts = datumErsterTagDesPrüfZyklus.AddDays(zeitUndOrts[z].Tag - 1);
                                                                        
                                    if (von <= tagDesUnterrichts && tagDesUnterrichts <= bis)
                                    {
                                        Unterricht unterricht = new Unterricht(
                                            id, 
                                            lehrer, 
                                            fach, 
                                            k.TrimEnd(','), 
                                            r, 
                                            "", 
                                            zeitUndOrts[z].Tag, 
                                            zeitUndOrts[z].Stunde, 
                                            unterrichtsgruppeDiesesUnterrichts, 
                                            datumErsterTagDesPrüfZyklus);
                                        this.Add(unterricht);
                                        try
                                        {
                                            string ugg = unterrichtsgruppeDiesesUnterrichts == null ? "" : unterrichtsgruppeDiesesUnterrichts.Name;
                                            Console.WriteLine(unterricht.Id.ToString().PadLeft(4) + " " + unterricht.LehrerKürzel.PadRight(4) + unterricht.KlasseKürzel.PadRight(20) + unterricht.FachKürzel.PadRight(10) + " Raum: " + r.PadRight(10) + " Tag: " + unterricht.Tag + " Stunde: " + unterricht.Stunde + " " + ugg.PadLeft(3));
                                        }
                                        catch (Exception ex)
                                        {

                                            throw;
                                        }                                        
                                    }
                                }                                
                            }
                        }
                    }
                    Console.WriteLine(("Unterrichte " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                    throw new Exception("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal List<Unterricht> SortierenUndKumulieren()
        {
            try
            {
                // Die Unterrichte werden chronologisch sortiert

                List<Unterricht> sortierteUnterrichts = (from u in this
                                                         orderby u.KlasseKürzel, u.FachKürzel, u.Raum, u.Tag, u.Stunde
                                                         select u).ToList();

                for (int i = 0; i < sortierteUnterrichts.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < sortierteUnterrichts.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (sortierteUnterrichts[i].KlasseKürzel == sortierteUnterrichts[i + 1].KlasseKürzel && sortierteUnterrichts[i].FachKürzel == sortierteUnterrichts[i + 1].FachKürzel && sortierteUnterrichts[i].Raum == sortierteUnterrichts[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (sortierteUnterrichts[i].Bis == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(15) == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(20) == sortierteUnterrichts[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                sortierteUnterrichts[i + 1].Von = sortierteUnterrichts[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                sortierteUnterrichts.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return (
                    from s in sortierteUnterrichts
                    orderby s.Tag, s.Stunde, s.KlasseKürzel
                    select s).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }            
        }

        internal Unterrichts FerienUndUnterrichtsgruppenUndVertretungsausfallFiltern(Lehrer lehrer, Feriens feriens, Substitutions substitutions, Abwesenheits abwesenheits, DateTime datumTagDerKalenderwoche)
        {
            try
            {
                Unterrichts unterrichteDiesesLehrers = new Unterrichts();
                
                foreach (var unterricht in (from u in this
                                            where u.LehrerKürzel == lehrer.Kürzel
                                            where u.Tag == (int)datumTagDerKalenderwoche.DayOfWeek
                                            orderby u.Tag
                                            orderby u.Stunde
                                            select u).ToList())
                {                    
                    bool fälltAusWeilKlasseAbsent = false;
                                        
                    foreach (var abwesenheit in abwesenheits)
                    {
                        if (abwesenheit.Klasse == unterricht.KlasseKürzel)
                        {
                            if (abwesenheit.Von.Date == unterricht.Von.Date)
                            {
                                if (abwesenheit.IsAllDayAbsence || (abwesenheit.VonStunde == unterricht.Stunde))
                                {
                                    fälltAusWeilKlasseAbsent = true;
                                }                                
                            }
                        }
                    }
                    
                    bool fälltAusWegenUnterrichtsgruppe = false;
                    
                    if (unterricht.Unterrichtsgruppe != null)
                    {
                        for (int i = 0; i < unterricht.Unterrichtsgruppe.Interruption.von.Count(); i++)
                        {
                            var montag = unterricht.Unterrichtsgruppe.Interruption.von[i];
                            var sonntag = unterricht.Unterrichtsgruppe.Interruption.bis[i].AddDays(-1);

                            if (montag <= datumTagDerKalenderwoche && datumTagDerKalenderwoche <= sonntag)
                            {
                                fälltAusWegenUnterrichtsgruppe = true;
                            }
                        }
                    }

                    bool fälltAusWegenFerien = false;

                    foreach (var ferien in feriens)
                    {
                        DateTime ersterMontagDerFerien = ferien.Von;
                        DateTime letzterSonntagInDenFerien = ferien.Bis;

                        if (ersterMontagDerFerien <= datumTagDerKalenderwoche && datumTagDerKalenderwoche <= letzterSonntagInDenFerien)
                        {
                            fälltAusWegenFerien = true;
                        }
                    }

                    bool fälltAusWegenVertretungsregelung = false;

                    foreach (var substitution in substitutions)
                    {
                        if (substitution.Datum == datumTagDerKalenderwoche)
                        {
                            if (substitution.Stunde == unterricht.Stunde)
                            {
                                if (substitution.BetroffenerUnterricht != null)
                                {
                                    if (substitution.BetroffenerUnterricht.Id == unterricht.Id)
                                    {
                                        if (substitution.ZuVertretender.Kürzel == unterricht.LehrerKürzel)
                                        {
                                            fälltAusWegenVertretungsregelung = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (fälltAusWeilKlasseAbsent || fälltAusWegenVertretungsregelung || fälltAusWegenFerien || fälltAusWegenUnterrichtsgruppe)
                    {
                        string anmerkung = "(" + (fälltAusWeilKlasseAbsent ? "wg. Kl. absent;" : "") + (fälltAusWegenVertretungsregelung ? "wg. Vertretung;" : "") + (fälltAusWegenFerien ? "wg. Ferien" : "") + (fälltAusWegenUnterrichtsgruppe ? " wg. Unterrichtsgruppe (" + (unterricht.Unterrichtsgruppe == null ? "" : unterricht.Unterrichtsgruppe.Name) + ")" : "") + ")";

                        Console.WriteLine(" [-] " + lehrer.Kürzel.PadRight(4) + datumTagDerKalenderwoche.ToShortDateString() + " " + datumTagDerKalenderwoche.DayOfWeek.ToString().PadRight(15) + " " + unterricht.Stunde + ". Stunde " + unterricht.KlasseKürzel.PadRight(6) + anmerkung);
                    }
                    else
                    {
                        unterrichteDiesesLehrers.Add(unterricht);

                        Console.WriteLine(" [ ] " + lehrer.Kürzel.PadRight(4) + unterricht.Von.ToShortDateString() + " " + unterricht.Von.DayOfWeek.ToString().PadRight(15) + " " + unterricht.Stunde + ". Stunde " + unterricht.KlasseKürzel.PadRight(6));
                    }
                }
                return unterrichteDiesesLehrers;
            }            
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }

        public Unterrichts Kumulieren()
        {
            try
            {
                for (int i = 0; i < this.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < this.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (this[i].KlasseKürzel == this[i + 1].KlasseKürzel && this[i].FachKürzel == this[i + 1].FachKürzel && this[i].Raum == this[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (this[i].Bis == this[i + 1].Von || this[i].Bis.AddMinutes(15) == this[i + 1].Von || this[i].Bis.AddMinutes(20) == this[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                this[i + 1].Von = this[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                this.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
    }
}