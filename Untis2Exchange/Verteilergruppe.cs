﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    class Verteilergruppe
    {
        public string Name { get; internal set; }
        public List<Lehrer> Lehrers { get; internal set; }

        public Verteilergruppe()
        {
            Lehrers = new List<Lehrer>();
        }

        public Verteilergruppe(string name, Lehrers lehrers)
        {
            this.Name = name;
            Lehrers = new List<Lehrer>();
            Lehrers = lehrers;
        }
    }
}
