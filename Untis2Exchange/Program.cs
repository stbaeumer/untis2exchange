﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace Untis2Exchange
{
    class Program
    {
        static void Main(string[] args)
        {
            Lehrers lehrers = new Lehrers();            
            int kalenderwoche = 0;
            Global.AdminMail = "stefan.baeumer@berufskolleg-borken.de";
            int anzahlWeitererWochen = 4;
            Tagesbericht.DatumTagesbericht = DateTime.Now;

            try
            {
                bool interactiveMode = true;

                do
                {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                    
                    Console.WriteLine("Untis2Exchange " + DateTime.Now);
                    Console.WriteLine("==================================");
                    Console.WriteLine("");
                    Console.WriteLine("Das Programm Untis2Exchange überträgt die Stunden- bzw. Vertretungspläne als Termine nach Exchange.");

                    List<string> aktSj = new List<string>();
                    string ausgabe = "";

                    do
                    {
                        try
                        {
                            Console.Write("Schuljahr eingeben (z. B. 2018) oder 10 Sekunden warten für alle oder Standardwert [" + Properties.Settings.Default.Schuljahr + "] und ENTER:");

                            string eingabe = Reader.ReadLine(15000).ToUpper();

                            // Wenn die Eingabe nicht leer ist, wird die Lehrerliste geleert.

                            if (eingabe != "")
                            {
                                aktSj = new List<string>();
                            }
                            else
                            {
                                eingabe = Properties.Settings.Default.Schuljahr.ToString();
                            }
                                                        
                            if (Convert.ToInt32(eingabe.Trim()) >= 2018 && Convert.ToInt32(eingabe.Trim()) < 9999)
                            {
                                aktSj.Add(eingabe.Trim());
                                aktSj.Add((Convert.ToInt32(eingabe.Trim()) + 1).ToString());
                            }
                            
                            Console.Write("Gewähltes Schuljahr: " + aktSj[0] + "/" + aktSj[1]);
                            Console.WriteLine("");
                        }
                        catch (TimeoutException)
                        {
                            aktSj = new List<string>();

                            aktSj.Add((DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1).ToString());
                            aktSj.Add((DateTime.Now.Month >= 8 ? DateTime.Now.Year + 1 : DateTime.Now.Year).ToString());

                            Console.WriteLine("");
                            Console.WriteLine("Aktuelles Schuljahr: " + aktSj[0] + "/" + aktSj[1]);

                            interactiveMode = false;
                        }
                    } while (Convert.ToInt32(aktSj[0]) >= 2018 && Convert.ToInt32(aktSj[0]) < 9999 ? false : true);

                    Properties.Settings.Default.Schuljahr = Convert.ToInt32(aktSj[0]);

                    Console.WriteLine("");

                    const string connectionString = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source=M:\\Data\\gpUntis.mdb;";

                    Feriens feriens = new Feriens(
                        aktSj[0] + aktSj[1], 
                        connectionString);

                    Periodes periodes = new Periodes(
                        aktSj[0] + aktSj[1], 
                        connectionString);

                    Fachs fachs = new Fachs(
                        aktSj[0] + aktSj[1], 
                        connectionString);

                    Unterrichtsgruppes unterrichtsgruppes = new Unterrichtsgruppes(
                        aktSj[0] + aktSj[1], 
                        connectionString);

                    lehrers = new Lehrers(
                        aktSj[0] + aktSj[1], 
                        connectionString, 
                        periodes);

                    kalenderwoche = Properties.Settings.Default.Kalenderwoche;

                    DateTime datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, DateTime.Now.Year);

                    List<string> lehrerkürzels = new List<string>();

                    foreach (var l in Properties.Settings.Default.Lehrer.Split(','))
                    {
                        lehrerkürzels.Add(l);
                    }

                    Console.WriteLine("");
                    
                    do
                    {
                        try
                        {
                            Console.Write("Lehrerkürzel (kommagetrennt) eingeben oder 10 Sekunden warten für alle oder Standardwert [" + Properties.Settings.Default.Lehrer + "] und ENTER:");

                            string eingabe = Reader.ReadLine(15000).ToUpper();

                            // Wenn die Eingabe nicht leer ist, wird die Lehrerliste geleert.

                            if (eingabe != "")
                            {
                                lehrerkürzels = new List<string>();
                            }
                            else
                            {
                                ausgabe = Properties.Settings.Default.Lehrer;
                            }

                            foreach (var e in eingabe.Split(','))
                            {
                                if ((from l in lehrers where l.Kürzel == e.Trim() select e).Any())
                                {
                                    lehrerkürzels.Add(e.Trim());
                                    ausgabe += e.Trim() + ",";
                                }
                            }

                            Console.Write(ausgabe.Substring(0, Math.Min(10, ausgabe.TrimEnd(',').Length)) + (ausgabe.Length > 10 ? " ..." : ""));
                            Console.WriteLine("");
                        }
                        catch (TimeoutException)
                        {
                            lehrerkürzels = new List<string>();

                            foreach (var lehrer in (from l in lehrers where l.Kürzel != "?" select l).ToList())
                            {
                                lehrerkürzels.Add(lehrer.Kürzel);
                                ausgabe += lehrer.Kürzel + ",";
                            }

                            Console.Write("alle");
                            Console.WriteLine("");

                            interactiveMode = false;
                        }
                    } while (!(from l in lehrers where l.Kürzel == (lehrerkürzels.Count == 0 ? "" : lehrerkürzels[0]) select l).Any());

                    if (ausgabe != "")
                    {
                        Properties.Settings.Default.Lehrer = ausgabe.TrimEnd(',');
                    }

                    Properties.Settings.Default.Save();

                    bool ok = false;

                    do
                    {
                        Console.Write("Kalenderwoche eingeben oder 10 Sekunden warten für aktuelle KW oder Standardwert [" + kalenderwoche + "] und ENTER. Es dürfen nur Kalenderwochen innerhalb von Perioden gewählt werden.");

                        try
                        {
                            string x = Reader.ReadLine(15000);

                            if (x == "")
                            {
                                kalenderwoche = Properties.Settings.Default.Kalenderwoche;
                                datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, Convert.ToInt32(kalenderwoche < 30 ? aktSj[1] : aktSj[0]));
                            }
                            else
                            {
                                kalenderwoche = Convert.ToInt32(x);
                                datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, Convert.ToInt32(kalenderwoche < 30 ? aktSj[1] : aktSj[0]));
                                Properties.Settings.Default.Kalenderwoche = kalenderwoche;
                                Properties.Settings.Default.Save();
                            }
                            ok = false;
                            Console.Write(kalenderwoche);
                        }
                        catch (TimeoutException)
                        {
                            CultureInfo CUI = CultureInfo.CurrentCulture;
                            kalenderwoche = CUI.Calendar.GetWeekOfYear(DateTime.Now, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
                            datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, DateTime.Now.Year);

                            // Ab freitags 14:30 beginnt die neue KW

                            if (DateTime.Now > datumMontagDerKalenderwoche.AddDays(4) + new TimeSpan(14, 30, 0))
                            {
                                kalenderwoche = CUI.Calendar.GetWeekOfYear(DateTime.Now, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek) + 1;
                                datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, DateTime.Now.Year);
                            }

                            ok = false;
                            Properties.Settings.Default.Kalenderwoche = kalenderwoche;
                            Properties.Settings.Default.Save();
                            Console.Write(kalenderwoche);
                            Console.WriteLine("");
                        }
                        catch (Exception)
                        {
                            ok = true;
                        }

                        if (periodes.OrderByDescending(i => i.Bis).FirstOrDefault().Bis < datumMontagDerKalenderwoche)
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Die gewünschte Kalenderwoche liegt nach der letzten Periode.");
                            Console.WriteLine("");
                            Console.ReadKey();
                            ok = true;
                        }

                        if (periodes.OrderBy(i => i.Von).FirstOrDefault().Von > datumMontagDerKalenderwoche)
                        {
                            Console.WriteLine("Die gewünschte Kalenderwoche liegt vor der ersten Periode.");
                        }

                        Console.WriteLine("");
                    } while (ok);
                    
                    for (int zähler = 0; zähler < anzahlWeitererWochen; zähler++)
                    {
                        datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, Convert.ToInt32(kalenderwoche < 30 ? aktSj[1] : aktSj[0]));

                        var periode = periodes.Count;

                        for (int p = 0; p < periodes.Count - 1; p++)
                        {
                            if (periodes[p].Von <= datumMontagDerKalenderwoche && datumMontagDerKalenderwoche < periodes[p + 1].Von)
                            {
                                periode = periodes[p].IdUntis;
                            }
                        }

                        if (kalenderwoche <= 52)
                        {
                            Klasses klasses = new Klasses(
                                aktSj[0] + aktSj[1],
                                lehrers,
                                connectionString, 
                                periode);

                            Raums raums = new Raums(
                                aktSj[0] + aktSj[1], 
                                connectionString, 
                                periode);
                            
                            Unterrichts unterrichts = new Unterrichts(
                                aktSj[0] + aktSj[1], 
                                datumMontagDerKalenderwoche, 
                                connectionString, 
                                periode, 
                                klasses, 
                                lehrers, 
                                fachs, 
                                raums, 
                                unterrichtsgruppes);

                            Substitutions substitutions = new Substitutions(
                                aktSj[0] + aktSj[1], 
                                connectionString, 
                                datumMontagDerKalenderwoche, 
                                lehrers, 
                                raums, 
                                fachs, 
                                klasses, 
                                unterrichts);

                            Pausenaufsichts pausenaufsichts = new Pausenaufsichts(
                                aktSj[0] + aktSj[1], 
                                periode, 
                                datumMontagDerKalenderwoche, 
                                lehrers,
                                feriens, 
                                connectionString);

                            Abwesenheits abwesenheits = new Abwesenheits(
                                aktSj[0] + aktSj[1],
                                datumMontagDerKalenderwoche,
                                substitutions,
                                lehrers,
                                klasses,
                                connectionString);

                            lehrers.ToExchange(
                                lehrerkürzels, 
                                datumMontagDerKalenderwoche, 
                                pausenaufsichts, 
                                feriens, 
                                substitutions, 
                                unterrichts, 
                                abwesenheits,
                                periode, 
                                periodes, 
                                kalenderwoche,
                                interactiveMode);                            
                        }
                        kalenderwoche++;
                    }

                    // Werktags um 8, 11 und 16 Uhr wird gesendet.

                    if ((!DateTime.Now.DayOfWeek.ToString().StartsWith("S") && DateTime.Now.Minute < 30 && (DateTime.Now.Hour == 8 || DateTime.Now.Hour == 11 || DateTime.Now.Hour == 16)))
                    {
                        Console.WriteLine("Tagesbericht wird gesendet.");
                        Tagesbericht.Senden();
                    }

                    Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());                    

                } while (interactiveMode);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Global.MailSenden("Fehler in Untis2Exchange", ex.ToString() + "Kalenderwoche:" + kalenderwoche);
                Console.ReadKey();
            }
        }

        private static DateTime GetMondayDateOfWeek(int week, int year)
        {
            int i = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(new DateTime(year, 1, 1), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            if (i == 1)
            {
                return CultureInfo.CurrentCulture.Calendar.AddDays(new DateTime(year, 1, 1), ((week - 1) * 7 - GetDayCountFromMonday(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1))) + 1));
            }
            else
            {
                int x = Convert.ToInt32(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1)));
                return CultureInfo.CurrentCulture.Calendar.AddDays(new DateTime(year, 1, 1), ((week - 1) * 7 + (7 - GetDayCountFromMonday(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1)))) + 1));
            }
        }

        private static int GetDayCountFromMonday(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                default:
                    //Sunday
                    return 7;
            }
        }
        
        public int GetCalendarWeek(DateTime date)
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            
            Calendar calendar = currentCulture.Calendar;
            
            int calendarWeek = calendar.GetWeekOfYear(date,
               currentCulture.DateTimeFormat.CalendarWeekRule,
               currentCulture.DateTimeFormat.FirstDayOfWeek);

            // Überprüfen, ob eine Kalenderwoche größer als 52
            // ermittelt wurde und ob die Kalenderwoche des Datums
            // in einer Woche 2 ergibt: In diesem Fall hat
            // GetWeekOfYear die Kalenderwoche nicht nach ISO 8601 
            // berechnet (Montag, der 31.12.2007 wird z. B.
            // fälschlicherweise als KW 53 berechnet). 
            // Die Kalenderwoche wird dann auf 1 gesetzt
            if (calendarWeek > 52)
            {
                date = date.AddDays(7);
                int testCalendarWeek = calendar.GetWeekOfYear(date,
                   currentCulture.DateTimeFormat.CalendarWeekRule,
                   currentCulture.DateTimeFormat.FirstDayOfWeek);
                if (testCalendarWeek == 2)
                    calendarWeek = 1;
            }
            return calendarWeek;
        }
    }
}