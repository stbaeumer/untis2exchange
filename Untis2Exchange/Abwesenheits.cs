﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Abwesenheits : List<Abwesenheit>
    {
        public Abwesenheits()
        {
        }

        public Abwesenheits(string aktSj, DateTime datumMontagDerKalenderwoche, Substitutions substitutions, Lehrers lehrers, Klasses klasses, string connectionString)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Absence.ABSENCE_ID, 
Absence.DateFrom, 
Absence.DateTo, 
Absence.Text, 
Absence.LessonFrom, 
Absence.LessonTo, 
Absence.TimeFrom, 
Absence.TimeTo, 
AbsenceReason.Longname, 
Absence.IDA,
Absence.TypeA, 
Event.Text
FROM (Absence LEFT JOIN Event ON Absence.EVENT_ID = Event.EVENT_ID) LEFT JOIN AbsenceReason ON Absence.ABSENCE_REASON_ID = AbsenceReason.ABSENCE_REASON_ID
WHERE (((Absence.SCHOOL_ID)=177659) AND ((Absence.SCHOOLYEAR_ID)=" + aktSj + ") AND ((Absence.VERSION_ID)=1) AND ((Absence.Deleted)=False) AND ((Absence.TypeA)=100 Or (Absence.TypeA)=101));";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();
                                        
                    while (oleDbDataReader.Read())
                    {                        
                        var idUntis = oleDbDataReader.GetInt32(0);
                                              
                        DateTime vonDatum = DateTime.ParseExact((oleDbDataReader.GetInt32(1)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime bisDatum = DateTime.ParseExact((oleDbDataReader.GetInt32(2)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        int vonStunde = oleDbDataReader.GetByte(4);
                        int bisStunde = oleDbDataReader.GetByte(5);
                        bool isAlldayAbsence = vonDatum != bisDatum ? true : vonStunde == 1 && bisStunde == 16 ? true : false;
                        DateTime von = GetVon(vonStunde, vonDatum, isAlldayAbsence);                    
                        DateTime bis = GetBis(bisStunde, bisDatum, isAlldayAbsence);                        
                        string langname = Global.SafeGetString(oleDbDataReader, 11) != "" ? Global.SafeGetString(oleDbDataReader, 11) : Global.SafeGetString(oleDbDataReader, 3) + " " + Global.SafeGetString(oleDbDataReader, 3);
                        string lehrerKürzel = "";
                        string klasse = "";

                        // Wenn eine Klasse absent ist, dann 
                        if (oleDbDataReader.GetByte(10) == 100)
                        {
                            lehrerKürzel = "";
                            klasse = (from k in klasses
                                      where oleDbDataReader.GetInt32(9) == k.IdUntis
                                      select k.NameUntis).FirstOrDefault();
                        }
                        if (oleDbDataReader.GetByte(10) == 101)
                        {
                            lehrerKürzel = (from l in lehrers
                                            where oleDbDataReader.GetInt32(9) == l.IdUntis
                                            select l.Kürzel).FirstOrDefault();
                            klasse = "";
                        }
                        
                        for (DateTime x = vonDatum; x.Date <= bisDatum; x = x.AddDays(1))
                        {
                            // Bei einem AlldayEvent werden Datum und Uhrzeiten für den Exchange angepasst.
                            if (isAlldayAbsence)
                            {
                                try
                                {
                                    vonStunde = 1;
                                    bisStunde = 16;
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }
                            Abwesenheit abwesenheit = new Abwesenheit()
                            {
                                IdUntis = idUntis,
                                LehrerKürzel = lehrerKürzel,
                                Klasse = klasse,
                                VonDatum = x,
                                BisDatum = x,
                                Von = GetVon(vonStunde, x, isAlldayAbsence),
                                Bis = GetBis(bisStunde, x, isAlldayAbsence),
                                VonStunde = vonStunde,
                                BisStunde = bisStunde,
                                IsAllDayAbsence = isAlldayAbsence,
                                Langname = langname
                            };

                            if (x >= datumMontagDerKalenderwoche && datumMontagDerKalenderwoche.AddDays(4) >= x)
                            {
                                if (langname != "")
                                {
                                    try
                                    {
                                        Console.WriteLine(lehrerKürzel.ToString().PadRight(4) + klasse.ToString().PadRight(7) + x.ToShortDateString() + " " + (isAlldayAbsence ? "" : vonStunde + "-" + bisStunde + " " + langname.PadRight(25)) + " " + (isAlldayAbsence ? "Ganzer Tag" : ""));
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                                                        
                                    this.Add(abwesenheit);
                                }
                            }

                            // Wenn der Lehrer am DatumTagesbericht abwesend ist, wird seine Abwesenheit erfasst.

                            if (vonDatum.Date <= Tagesbericht.DatumTagesbericht.Date && Tagesbericht.DatumTagesbericht.Date <= bisDatum.Date)
                            {
                                Tagesbericht.AddAbwesende(abwesenheit, Tagesbericht.DatumTagesbericht);
                            } 
                        }
                    };

                    Console.WriteLine(("Abwesenheiten " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        private DateTime GetBis(int bisStunde, DateTime bisDatum, bool isAlldayAbsence)
        {
            if (isAlldayAbsence)
            {
                return bisDatum.Date + new TimeSpan(24, 00, 0);
            }
            TimeSpan ts = new TimeSpan();

            switch (bisStunde)
            {
                case 1:
                    ts = new TimeSpan(8, 25, 0);
                    break;
                case 2:
                    ts = new TimeSpan(9, 10, 0);
                    break;
                case 3:
                    ts = new TimeSpan(10, 15, 0);
                    break;
                case 4:
                    ts = new TimeSpan(11, 00, 0);
                    break;
                case 5:
                    ts = new TimeSpan(12, 00, 0);
                    break;
                case 6:
                    ts = new TimeSpan(12, 45, 0);
                    break;
                case 7:
                    ts = new TimeSpan(13, 45, 0);
                    break;
                case 8:
                    ts = new TimeSpan(14, 30, 0);
                    break;
                case 9:
                    ts = new TimeSpan(15, 30, 0);
                    break;
                case 10:
                    ts = new TimeSpan(16, 15, 0);
                    break;
                case 11:
                    ts = new TimeSpan(17, 00, 0);
                    break;
                case 12:
                    ts = new TimeSpan(18, 0, 0);
                    break;
                case 13:
                    ts = new TimeSpan(19, 0, 0);
                    break;
                case 14:
                    ts = new TimeSpan(19, 55, 0);
                    break;
                case 16:
                    ts = new TimeSpan(24, 00, 0);
                    break;
            }

            return bisDatum.Date + ts;
        }

        private DateTime GetVon(int vonStunde, DateTime vonDatum, bool isAlldayAbsence)
        {
            if (isAlldayAbsence)
            {
                return vonDatum.Date;
            }

            TimeSpan ts = new TimeSpan();

            switch (vonStunde)
            {
                case 1:
                    ts = new TimeSpan(7, 40, 0);
                    break;
                case 2:
                    ts = new TimeSpan(8, 25, 0);
                    break;
                case 3:
                    ts = new TimeSpan(9, 30, 0);
                    break;
                case 4:
                    ts = new TimeSpan(10, 15, 0);
                    break;
                case 5:
                    ts = new TimeSpan(11, 15, 0);
                    break;
                case 6:
                    ts = new TimeSpan(12, 0, 0);
                    break;
                case 7:
                    ts = new TimeSpan(13, 0, 0);
                    break;
                case 8:
                    ts = new TimeSpan(13, 45, 0);
                    break;
                case 9:
                    ts = new TimeSpan(14, 45, 0);
                    break;
                case 10:
                    ts = new TimeSpan(15, 30, 0);
                    break;
                case 11:
                    ts = new TimeSpan(16, 30, 0);
                    break;
                case 12:
                    ts = new TimeSpan(18, 0, 0);
                    break;
                case 13:
                    ts = new TimeSpan(19, 0, 0);
                    break;
                case 14:
                    ts = new TimeSpan(19, 55, 0);
                    break;
            }            
            return vonDatum.Date + ts;
        }
    }
}
