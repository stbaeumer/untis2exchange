﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Abwesenheit
    {
        public int IdUntis { get; internal set; }
        public DateTime VonDatum { get; internal set; }
        public DateTime BisDatum { get; internal set; }
        public int VonStunde { get; internal set; }
        public int BisStunde { get; internal set; }
        public bool IsAllDayAbsence { get; internal set; }
        public string Langname { get; internal set; }
        public string LehrerKürzel { get; internal set; }
        public DateTime Von { get; internal set; }
        public DateTime Bis { get; internal set; }
        public string Klasse { get; internal set; }
    }
}
