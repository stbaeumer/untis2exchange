﻿using Microsoft.Exchange.WebServices.Data;
using System;

namespace Untis2Exchange
{
    public class MyAppointment
    {
        private string lehrerKürzel;
        public string LehrerKürzel
        {
            get { return lehrerKürzel; }
            set { lehrerKürzel = value; }
        }
                
        ExchangeService service = new ExchangeService();

        private Appointment a;

        public Appointment A
        {
            get { return a; }
            set { a = value; }
        }

        public bool Delete { get; set; }

        public MyAppointment(DateTime beginnUhrzeit, DateTime endeUhrzeit, string ort, string lehrerKürzel, string betreff, string beschreibung, int rowspan, int colspan, ExchangeService service, bool delete)
        {
            this.LehrerKürzel = lehrerKürzel;
            
            this.A = new Appointment(service);

            this.A.Start = beginnUhrzeit;
            this.A.End = endeUhrzeit;

            this.A.IsAllDayEvent = false;

            if (beginnUhrzeit.Hour == 7 && beginnUhrzeit.Minute == 40 && endeUhrzeit.Hour == 18 && endeUhrzeit.Minute == 30)
            {
                this.A.IsAllDayEvent = true;
                this.A.Start = new DateTime(beginnUhrzeit.Year, beginnUhrzeit.Month, beginnUhrzeit.Day, 0, 0, 0);
                this.A.End = this.A.Start.AddHours(24);
            }
            this.A.IsReminderSet = false;

            this.A.Location = ort;

            this.A.Subject = betreff;
            this.A.Body = beschreibung;

            this.Delete = delete;

            this.A.Categories.Add("Stundenplan");

        }

        public MyAppointment()
        {
            this.A = new Appointment(service);
        }
    }
}