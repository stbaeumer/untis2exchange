﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public interface ITextRow
    {
        String Output();
        void Output(StringBuilder sb);
        Object Tag { get; set; }
    }

    public class TableBuilder : IEnumerable<ITextRow>
    {
        protected class TextRow : List<String>, ITextRow
        {
            protected TableBuilder owner = null;
            public TextRow(TableBuilder Owner)
            {
                owner = Owner;
                if (owner == null) throw new ArgumentException("Owner");
            }
            public String Output()
            {
                StringBuilder sb = new StringBuilder();
                Output(sb);
                return sb.ToString();
            }
            public void Output(StringBuilder sb)
            {
                sb.AppendFormat(owner.FormatString, this.ToArray());
            }
            public Object Tag { get; set; }
        }

        public String Separator { get; set; }

        protected List<ITextRow> rows = new List<ITextRow>();
        protected List<int> colLength = new List<int>();

        public TableBuilder()
        {
            Separator = "  ";
        }

        public TableBuilder(String separator)
            : this()
        {
            Separator = separator;
        }

        public TableBuilder(string lehrerKürzel, Periodes periodes, int periode, int kalenderwoche, List<Unterricht> unterrichteDiesesLehrers, Pausenaufsichts pausenaufsichtsDiesesLehrers, Abwesenheits abwesenheitsDiesesLehres, DateTime datumErsterTagDesPrüfzyklus) : this()
        {
            try
            {
                Console.WriteLine("");
                Console.WriteLine(lehrerKürzel + " KW " + kalenderwoche + " (" + datumErsterTagDesPrüfzyklus.ToShortDateString() + "-" + datumErsterTagDesPrüfzyklus.AddDays(6).ToShortDateString() + ") Periode:" + periode + " (" + (from p in periodes where p.IdUntis == periode select p.Von.ToShortDateString()).FirstOrDefault() + "-" + (from p in periodes where p.IdUntis == periode select p.Bis.ToShortDateString()).FirstOrDefault() + ")");

                Console.WriteLine(this.AddRow("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
                this.AddRow("===", "====", "====", "====", "====", "====", "====", "====", "====", "====", "====");

                for (int schultag = 1; schultag <= 5; schultag++)
                {
                    string[] schultagArrayKlasse = new string[11];
                    string[] schultagArrayFach = new string[11];
                    string[] schultagArrayRaum = new string[11];

                    List<Unterricht> schultagStunden = (from u in unterrichteDiesesLehrers where u.Tag == schultag select u).ToList();

                    schultagArrayKlasse[0] = datumErsterTagDesPrüfzyklus.AddDays(schultag - 1).DayOfWeek.ToString().Substring(0, 2);
                    schultagArrayFach[0] = "";
                    schultagArrayRaum[0] = "";

                    for (int schulstunde = 1; schulstunde <= 10; schulstunde++)
                    {
                        string k = "";
                        for (int c = 0; c < (from v in schultagStunden where v.Stunde == schulstunde select v).Count(); c++)
                        {
                            k += (from v in schultagStunden where v.Stunde == schulstunde select v.KlasseKürzel).ToList()[c] == null ? " ".PadRight(6) + " |" : (from v in schultagStunden where v.Stunde == schulstunde select v.KlasseKürzel).ToList()[c].PadRight(6) + " |";
                        }

                        schultagArrayKlasse[schulstunde] = k.TrimEnd('|');

                        string f = "";
                        for (int c = 0; c < (from v in schultagStunden where v.Stunde == schulstunde select v).Count(); c++)
                        {
                            f += (from v in schultagStunden where v.Stunde == schulstunde select v.FachKürzel).ToList()[c] == null ? " ".PadRight(6) + " |" : (from v in schultagStunden where v.Stunde == schulstunde select v.FachKürzel).ToList()[c].PadRight(6) + " |";
                        }

                        schultagArrayFach[schulstunde] = f.TrimEnd('|');

                        string r = "";
                        for (int c = 0; c < (from v in schultagStunden where v.Stunde == schulstunde select v).Count(); c++)
                        {
                            r += (from v in schultagStunden where v.Stunde == schulstunde select v.Raum).ToList()[c] == null ? " ".PadRight(6) + " |" : (from v in schultagStunden where v.Stunde == schulstunde select v.Raum).ToList()[c].PadRight(6) + " |";
                        }

                        schultagArrayRaum[schulstunde] = r.TrimEnd('|');
                    }

                    this.AddRow(schultagArrayKlasse);
                    this.AddRow(schultagArrayFach);
                    this.AddRow(schultagArrayRaum);
                    this.AddRow("", "", "", "", "", "", "", "", "", "", "");
                }
                Console.WriteLine("");
                Console.Write(this.Output());
                                
                foreach (var item in pausenaufsichtsDiesesLehrers.OrderBy(x=>x.Beginn))
                {
                    if (item.LehrerKürzel == lehrerKürzel)
                    {
                        Console.WriteLine(item.Corridorname + "(" + item.Beginn.ToShortDateString() + " " + item.Beginn.ToShortTimeString() + "-" + item.Ende + ")");
                    }
                }
                                
                foreach (var item in abwesenheitsDiesesLehres)
                {
                    if (item.LehrerKürzel == lehrerKürzel)
                    {
                        Console.WriteLine(item.Langname + " " + item.IdUntis + " (" + item.Von.ToShortDateString() + " " + item.Von.ToShortTimeString() + "-" + item.Bis + ")");
                    }
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }            
        }

        public ITextRow AddRow(params object[] cols)
        {
            TextRow row = new TextRow(this);
            foreach (object o in cols)
            {
                String str = o.ToString().Trim();
                row.Add(str);
                if (colLength.Count >= row.Count)
                {
                    int curLength = colLength[row.Count - 1];
                    if (str.Length > curLength) colLength[row.Count - 1] = str.Length;
                }
                else
                {
                    colLength.Add(str.Length);
                }
            }
            rows.Add(row);
            return row;
        }

        protected String _fmtString = null;        
        public String FormatString
        {
            get
            {
                if (_fmtString == null)
                {
                    String format = "";
                    int i = 0;
                    foreach (int len in colLength)
                    {
                        format += String.Format("{{{0},-{1}}}{2}", i++, len, Separator);
                    }
                    format += "\r\n";
                    _fmtString = format;
                }
                return _fmtString;
            }
        }

        public String Output()
        {
            StringBuilder sb = new StringBuilder();
            foreach (TextRow row in rows)
            {
                row.Output(sb);
            }
            return sb.ToString();
        }

        #region IEnumerable Members

        public IEnumerator<ITextRow> GetEnumerator()
        {
            return rows.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return rows.GetEnumerator();
        }

        #endregion
    }
}
    
