﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Fach
    {
        public int IdUntis { get; internal set; }
        public string KürzelUntis { get; internal set; }
        public string LangnameUntis { get; internal set; }
        public string BezeichnungImZeugnis { get; internal set; }
        public string Fachklassen { get; internal set; }
    }
}
