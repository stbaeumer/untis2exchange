﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace Untis2Exchange
{
    public class Lehrers : List<Lehrer>
    {
        public Lehrers()
        {
        }

        public Lehrers(string aktSj, string connectionString, Periodes periodes)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Teacher.Teacher_ID, 
Teacher.Name, 
Teacher.Longname, 
Teacher.FirstName,
Teacher.Email,
Teacher.PlannedWeek
FROM Teacher 
WHERE (((SCHOOLYEAR_ID)= " + aktSj + ") AND  ((TERM_ID)=" + periodes.Count + ") AND ((Teacher.SCHOOL_ID)=177659) AND (((Teacher.Deleted)=No))) ORDER BY Teacher.Name;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        Lehrer lehrer = new Lehrer()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            Kürzel = Global.SafeGetString(oleDbDataReader, 1),
                            Mail = Global.SafeGetString(oleDbDataReader, 4)
                        };

                        if(!lehrer.Mail.EndsWith("@berufskolleg-borken.de") && lehrer.Kürzel != "LAT" && lehrer.Kürzel != "?")
                            Global.MailSenden("Untis2Exchange Fehlermeldung", "Der Lehrer " + lehrer.Kürzel + " hat keine Mail-Adresse in Untis. Bitte in Untis eintragen.");

                        this.Add(lehrer);
                    };
                    
                    Console.WriteLine(("Lehrer " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');
                    
                    oleDbDataReader.Close();                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        public void ToExchange(
            List<string> lehrerkürzels,
            DateTime datumMontagDerKalenderwoche, 
            Pausenaufsichts pausenaufsichts, 
            Feriens feriens, 
            Substitutions substitutions, 
            Unterrichts unterrichts, 
            Abwesenheits abwesenheits,
            int periode, 
            Periodes periodes, 
            int kalenderwoche,
            bool interactiveMode)
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013)
            {
                UseDefaultCredentials = true
            };

            service.TraceEnabled = false;
            service.TraceFlags = TraceFlags.All;
            service.Url = new Uri("https://ex01.bkb.local/EWS/Exchange.asmx");

            foreach (var lehrer in (from l in this
                                    where lehrerkürzels.Contains(l.Kürzel)
                                    where l.Mail.ToLower().EndsWith("@berufskolleg-borken.de")
                                    where l.Kürzel != "?"
                                    select l
                                    ))
            {
                lehrer.ToExchange(
                    datumMontagDerKalenderwoche, 
                    pausenaufsichts, 
                    feriens, 
                    substitutions, 
                    unterrichts, 
                    abwesenheits,
                    periode, 
                    service, 
                    periodes, 
                    kalenderwoche);

                if (interactiveMode)
                {
                    bool weiter = true;

                    do
                    {
                        if (weiter)
                        {
                            try
                            {
                                string eingabe = Reader.ReadLine(20000).ToUpper();
                                weiter = !weiter;
                            }
                            catch (TimeoutException)
                            {
                                weiter = true;
                            }
                        }
                        else
                        {
                            Console.ReadKey();
                            weiter = true;
                        }                    
                    } while (!weiter);
                }
            }
        }
    }
}