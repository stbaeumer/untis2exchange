﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Pausenaufsicht
    {
        public int IdUntis { get; internal set; }
        public string Corridorname { get; internal set; }
        public int Dauer { get; internal set; }
        public int Tag { get; internal set; }
        public DateTime Beginn { get; internal set; }
        public DateTime Ende { get; internal set; }
        public int Periode { get; internal set; }
        public string LehrerKürzel { get; internal set; }
        public int Stunde { get; internal set; }
        public string Text { get; private set; }        
        private DateTime DatumMontagDerKalenderwoche;

        public Pausenaufsicht(int idUntis, int periode, string kürzel, string corridorname, int dauer, string text, int tag, int stunde, DateTime datumMontagDerKalenderwoche)
        {
            this.IdUntis = idUntis;
            this.Periode = periode;
            this.LehrerKürzel = kürzel;
            this.Corridorname = corridorname;
            this.Dauer = dauer;
            this.Text = text;
            this.Tag = tag;
            this.Stunde = stunde;
            this.DatumMontagDerKalenderwoche = datumMontagDerKalenderwoche;
            this.Corridorname = corridorname;
            this.Beginn = GetBeginn(stunde, tag, datumMontagDerKalenderwoche);
            this.Ende = GetEnde(stunde, tag, datumMontagDerKalenderwoche);
        }

        public Pausenaufsicht()
        {
        }

        private DateTime GetBeginn(int stunde, int tag, DateTime ersterMontagDerKalenderwoche)
        {
            TimeSpan beginn = new TimeSpan();

            if (stunde == 1)
            {
                beginn = new TimeSpan(7, 30, 0);
            }
            if (stunde == 3)
            {
                beginn = new TimeSpan(9, 10, 0);
            }
            if (stunde == 5)
            {
                beginn = new TimeSpan(11, 0, 0);
            }
            if (stunde == 7)
            {
                beginn = new TimeSpan(12, 45, 0);
            }

            return ersterMontagDerKalenderwoche.AddDays(tag - 1) + beginn;
        }

        private DateTime GetEnde(int stunde, int tag, DateTime ersterMontagDerKalenderwoche)
        {
            TimeSpan ende = new TimeSpan();

            if (stunde == 1)
            {
                ende = new TimeSpan(7, 40, 0);
            }
            if (stunde == 3)
            {
                ende = new TimeSpan(9, 30, 0);
            }
            if (stunde == 5)
            {
                ende = new TimeSpan(11, 15, 0);
            }
            if (stunde == 7)
            {
                ende = new TimeSpan(13, 0, 0);
            }
            return ersterMontagDerKalenderwoche.AddDays(tag - 1) + ende;
        }
    }
}
