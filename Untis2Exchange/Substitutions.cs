﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Substitutions : List<Substitution>
    {
        public Substitutions(string aktSj, string connectionString, DateTime datumErsterTagDdatumMontagDerKalenderwoche, Lehrers lehrers, Raums raums, Fachs fachs, Klasses klasses, Unterrichts unterrichts)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Substitution.SCHOOLYEAR_ID, 
Substitution.Date, 
Substitution.TEACHER_IDSubst, 
Substitution.ROOM_IDSubst,  
Substitution.LESSON_IDSubst, 
Substitution.TEACHER_IDLessn,
Substitution.Flags,
Substitution.SUBJECT_IDSubst, 
Substitution.TRANSFER_ID, 
Substitution.ClassIds, 
Substitution.Lesson,
Substitution.SUBSTITUTION_ID,
Substitution.Text,
Substitution.BreakSubstData,
Substitution.Corridor_ID,
Corridor.Name,
Substitution.AbsenceIds
FROM Corridor Right JOIN Substitution ON Corridor.CORRIDOR_ID = Substitution.CORRIDOR_ID
WHERE (((Substitution.SCHOOLYEAR_ID)=" + aktSj + ") AND ((Substitution.SCHOOL_ID)=177659) AND ((Substitution.Date)>=" + datumErsterTagDdatumMontagDerKalenderwoche.ToString("yyyyMMdd") + ") AND ((Substitution.Date)<=" + datumErsterTagDdatumMontagDerKalenderwoche.AddDays(5).ToString("yyyyMMdd") + "));";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        Klasse klasse = new Klasse();
                        try
                        {
                            klasse = (from k in klasses where k.IdUntis == Convert.ToInt32((Global.SafeGetString(oleDbDataReader, 9)).Split(',')[1]) select k).FirstOrDefault();
                        }
                        catch (Exception)
                        {                            
                        }

                        DateTime datum = DateTime.ParseExact((oleDbDataReader.GetInt32(1)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                        Unterricht betroffenerUnterricht = new Unterricht();

                        try
                        {
                            betroffenerUnterricht = (from u in unterrichts
                                                     where u.Id == oleDbDataReader.GetInt32(4)
                                                     where u.Von.Date == datum.Date
                                                     select u).FirstOrDefault();
                        }
                        catch (Exception)
                        {                            
                        }

                        List<int> absenceIds = new List<int>();
                        foreach (var item in (Global.SafeGetString(oleDbDataReader, 16)).Split(','))
                        {
                            if (item != "")
                            {
                                absenceIds.Add(Convert.ToInt32(item));
                            }
                        }

                        string art = (Global.SafeGetString(oleDbDataReader, 6)).Substring(0, 1);

                        if (art == "E")
                        {
                            art = "Entfall";
                        }
                        if (art == "F")
                        {
                            art = "Freisetzung";
                        }
                        if (art == "B")
                        {
                            art = "Betreuung";
                        }
                        if (art == "S")
                        {
                            art = "Sondereinsatz";
                        }
                        if (art == "r")
                        {
                            art = "Verlegung";
                        }
                        Substitution substitution = new Substitution()
                        {
                            Datum = datum,
                            Vertreter = (from l in lehrers where l.IdUntis == oleDbDataReader.GetInt32(2) select l).FirstOrDefault(), 
                            Raum = (from r in raums where r.IdUntis == oleDbDataReader.GetInt32(3) select r.Raumnummer).FirstOrDefault(),
                            BetroffenerUnterricht = betroffenerUnterricht,
                            ZuVertretender = (from l in lehrers where l.IdUntis == oleDbDataReader.GetInt32(5) select l).FirstOrDefault(),
                            Art = art,
                            Fach = (from f in fachs where f.IdUntis == oleDbDataReader.GetInt32(7) select f.KürzelUntis).FirstOrDefault(),
                            Transfer_ID = oleDbDataReader.GetInt32(8),
                            Klasse = klasse,
                            Stunde = oleDbDataReader.GetByte(10),
                            ID = oleDbDataReader.GetInt32(11),
                            Text = Global.SafeGetString(oleDbDataReader, 12),
                            BreakSubst = Global.SafeGetString(oleDbDataReader, 13),
                            Corridor_ID = oleDbDataReader.GetInt32(14),
                            CorridorName = Global.SafeGetString(oleDbDataReader, 15),
                            Absence_Ids = absenceIds
                        };
                        
                        string zuVertretender = substitution.ZuVertretender == null ? "" : substitution.ZuVertretender.Kürzel;
                        
                        string vertreter = substitution.Vertreter == null ? "" : substitution.Vertreter.Kürzel;

                        string betrUnt = betroffenerUnterricht == null ? "" : betroffenerUnterricht.Id.ToString().PadRight(6) ?? "";
                        
                        if (betroffenerUnterricht != null)
                        {
                            if (substitution.Fach == null)
                            {
                                substitution.Fach = betroffenerUnterricht.FachKürzel;
                            }
                            if (substitution.Klasse == null)
                            {
                                substitution.Klasse = (from k in klasses where betroffenerUnterricht.KlasseKürzel == k.NameUntis select k).FirstOrDefault();
                            }
                        }

                        if (art == "Freisetzung" || art == "Entfall")
                        {
                            Console.WriteLine(substitution.Datum.ToShortDateString() + " " + betrUnt + " Art:" + substitution.Art.PadRight(10) + " Fach:" + substitution.Fach + " Text:" + substitution.Text.PadRight(20) + substitution.CorridorName);
                            // Bei Freisezung oder Entfall wird der Lehrer entfernt.
                            substitution.Vertreter = new Lehrer();
                            this.Add(substitution);
                        }
                        else
                        {
                            Console.WriteLine(substitution.Datum.ToShortDateString() + " " + betrUnt + " Entfällt:" + zuVertretender.PadRight(5) + " Vertreter:" + vertreter + " Fach:" + substitution.Fach + " Text:" + substitution.Text.PadRight(20) + substitution.CorridorName);                            
                            this.Add(substitution);
                        }
                    };

                    Console.WriteLine(("Vertretungen " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal Unterrichts UnterrichtsVertretungenHinzufügen(Lehrer lehrer, DateTime datumMontagDerKalenderwoche, DateTime datumTagDerKalenderwoche)
        {
            Unterrichts unterrichteDiesesLehrers = new Unterrichts();

            try
            {
                foreach (var substitution in this)
                {
                    if (substitution.Datum.Date == datumTagDerKalenderwoche.Date)
                    {
                        if (substitution.Vertreter != null)
                        {
                            if (substitution.Vertreter.Kürzel == lehrer.Kürzel)
                            {
                                string klasse = substitution.Klasse == null ? "" : klasse = substitution.Klasse.NameUntis;

                                if (substitution.Corridor_ID == 0)
                                {
                                    unterrichteDiesesLehrers.Add(new Unterricht(000, lehrer.Kürzel, substitution.Fach, klasse, substitution.Raum, substitution.Text, (int)substitution.Datum.DayOfWeek, substitution.Stunde, null, datumMontagDerKalenderwoche));
                                    Console.WriteLine(" [+] " + lehrer.Kürzel.PadRight(4) + datumTagDerKalenderwoche.ToShortDateString() + " " + datumTagDerKalenderwoche.DayOfWeek.ToString().PadRight(15) + " " + substitution.Stunde + ". Stunde " + (klasse == null ? " ".PadRight(6) : klasse.PadRight(6)) + " statt:" + (substitution.ZuVertretender == null ? "" : substitution.ZuVertretender.Kürzel) + "(" + substitution.Fach + ") " + substitution.Text);
                                }
                            }
                        }            
                    }                    
                }
                return unterrichteDiesesLehrers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }

        internal Pausenaufsichts PausenaufsichtsVertretungenHinzufügen(Lehrer lehrer, DateTime datumMontagDerKalenderwoche, DateTime datumTagDerKalenderwoche, int periode)
        {
            Pausenaufsichts pausenaufsichtsDiesesLehrers = new Pausenaufsichts();

            try
            {
                foreach (var substitution in this)
                {
                    if (substitution.Datum.Date == datumTagDerKalenderwoche.Date)
                    {
                        if (substitution.Vertreter != null)
                        {
                            if (substitution.Vertreter.Kürzel == lehrer.Kürzel)
                            {
                                string klasse = substitution.Klasse == null ? "" : klasse = substitution.Klasse.NameUntis;

                                if (substitution.Corridor_ID != 0)
                                {
                                    pausenaufsichtsDiesesLehrers.Add(new Pausenaufsicht(0, periode, lehrer.Kürzel, substitution.CorridorName, 0, substitution.Text, (int)substitution.Datum.DayOfWeek, substitution.Stunde, datumMontagDerKalenderwoche));
                                    Console.WriteLine(" [+] " + lehrer.Kürzel.PadRight(4) + datumTagDerKalenderwoche.ToShortDateString() + " " + datumTagDerKalenderwoche.DayOfWeek.ToString().PadRight(15) + " " + substitution + ". Stunde Gebäude: " + substitution.Corridor_ID);
                                }
                            }
                        }                        
                    }
                }
                return pausenaufsichtsDiesesLehrers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
    }
}
