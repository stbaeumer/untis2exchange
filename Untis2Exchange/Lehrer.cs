﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Lehrer
    {
        public int IdUntis { get; internal set; }
        public string Kürzel { get; internal set; }
        public string Mail { get; internal set; }

        internal void ToExchange(
            DateTime datumMontagDerKalenderwoche, 
            Pausenaufsichts pausenaufsichts, 
            Feriens feriens, 
            Substitutions substitutions, 
            Unterrichts unterrichts, 
            Abwesenheits abwesenheits,
            int periode, 
            ExchangeService service, 
            Periodes periodes, 
            int kalenderwoche)
        {
            try
            {
                var unterrichteDiesesLehrers = new Unterrichts();
                var abwesenheitenDiesesLehres = new Abwesenheits();
                var pausenaufsichtsDiesesLehrers = new Pausenaufsichts();

                Console.WriteLine("Alle Unterrichte (unter Berücksichtigung v. Vertretungen, Ausfällen, Pausenaufsichten) des Lehres " + this.Kürzel +" in der KW " + kalenderwoche + ".");

                for (DateTime datumTagDerKalenderwoche = datumMontagDerKalenderwoche; datumTagDerKalenderwoche.Date <= datumMontagDerKalenderwoche.AddDays(4); datumTagDerKalenderwoche = datumTagDerKalenderwoche.AddDays(1))
                {                    
                    abwesenheitenDiesesLehres.AddRange((from a in abwesenheits
                                                        where a.LehrerKürzel == this.Kürzel
                                                        where a.VonDatum.Date == datumTagDerKalenderwoche.Date
                                                        select a));

                    pausenaufsichtsDiesesLehrers.AddRange(pausenaufsichts.AusfälleHerausfiltern(
                        this,
                        feriens,
                        substitutions,
                        datumTagDerKalenderwoche));

                    unterrichteDiesesLehrers.AddRange(unterrichts.FerienUndUnterrichtsgruppenUndVertretungsausfallFiltern(
                        this,
                        feriens,
                        substitutions,
                        abwesenheits,
                        datumTagDerKalenderwoche));

                    unterrichteDiesesLehrers.AddRange(
                        substitutions.UnterrichtsVertretungenHinzufügen(
                            this,
                            datumMontagDerKalenderwoche,
                            datumTagDerKalenderwoche));

                    pausenaufsichtsDiesesLehrers.AddRange(
                        substitutions.PausenaufsichtsVertretungenHinzufügen(
                            this,
                            datumMontagDerKalenderwoche,
                            datumTagDerKalenderwoche,
                            periode));

                    // Wenn der Lehrer an diesem Tag keinen Unterricht hat, wird das erfasst.
                    
                    if (datumTagDerKalenderwoche.Date == Tagesbericht.DatumTagesbericht.Date && 
                        unterrichteDiesesLehrers.Count() == 0 && 
                        pausenaufsichtsDiesesLehrers.Count() == 0)
                    {                        
                        Tagesbericht.AddLehrerOhneUnterrichtUndOhnePausenaufsicht(
                            this, 
                            datumTagDerKalenderwoche);
                    }
                }

                service.ImpersonatedUserId = new ImpersonatedUserId(
                    ConnectingIdType.SmtpAddress, 
                    this.Mail);

                Appointments appointmentsIst = new Appointments(
                    this.Mail,
                    datumMontagDerKalenderwoche,
                    datumMontagDerKalenderwoche.AddDays(5),
                    service);
                                
                Appointments appointmentsSoll = new Appointments(
                    unterrichteDiesesLehrers,
                    pausenaufsichtsDiesesLehrers,
                    abwesenheitenDiesesLehres,
                    feriens,
                    datumMontagDerKalenderwoche,
                    datumMontagDerKalenderwoche.AddDays(5),
                    service);

                appointmentsIst.DeleteAppointments(
                    appointmentsSoll);

                appointmentsSoll.AddAppointments(
                    appointmentsIst,
                    this,
                    service);

                TableBuilder tb = new TableBuilder(
                    this.Kürzel,
                    periodes,
                    periode,
                    kalenderwoche,
                    unterrichteDiesesLehrers,
                    pausenaufsichtsDiesesLehrers,
                    abwesenheitenDiesesLehres,
                    datumMontagDerKalenderwoche);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler bei Lehrer " + this.Kürzel + "\n" + ex.ToString());
                throw new Exception("Fehler bei Lehrer " + this.Kürzel + "\n" + ex.ToString());
            }
        }
    }
}
