﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Substitution
    {
        public Lehrer Vertreter { get; internal set; }
        public DateTime Datum { get; internal set; }
        public string Raum { get; internal set; }
        public Lehrer ZuVertretender { get; internal set; }
        
        // L Pausenaufsicht
        // r Verlegung
        // F Freisetzung
        // E Entfall
        // B Betreuung
        // S Sondereinsatz
        public string Art { get; internal set; }
        public string Fach { get; internal set; }
        public int Transfer_ID { get; internal set; }
        public int Stunde { get; internal set; }
        public int ID { get; internal set; }
        public string Text { get; internal set; }
        public string BreakSubst { get; internal set; }
        public int Corridor_ID { get; internal set; }
        public string CorridorName { get; internal set; }
        public List<int> Absence_Ids { get; internal set; }


        // Bei Verlegungen ist die Transfer_ID nicht 0

        internal Unterricht BetroffenerUnterricht { get; set; }
        internal Klasse Klasse { get; set; }
    }
}
