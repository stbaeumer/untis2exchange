﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    class Verteilergruppes : List<Verteilergruppe>
    {
        private Unterrichts Unterrichts;
        private Klasses Klasses;

        public Verteilergruppes(Klasses klasses, Lehrers lehrers, Unterrichts unterrichts)
        {
            try
            {
                this.Klasses = klasses;
                this.Unterrichts = unterrichts;

                // Aus jeder Klasse wird eine Verteilergruppe

                foreach (var klasse in klasses)
                {
                    Verteilergruppe verteilergruppe = new Verteilergruppe();
                    verteilergruppe.Name = (Regex.IsMatch(klasse.NameUntis, @"^\d+")) ? "FOS" + klasse.NameUntis : klasse.NameUntis;

                    // Alle Lehrer, die in dieser Klasse unterrichten werden der VG hinzugefügt.

                    verteilergruppe.Lehrers = new List<Lehrer>();

                    verteilergruppe.Lehrers.AddRange((from u in unterrichts
                                                      where u.KlasseKürzel == klasse.NameUntis
                                                      select (from l in lehrers
                                                              where l.Kürzel == u.LehrerKürzel
                                                              select l).FirstOrDefault()).ToList().Distinct());
                    this.Add(verteilergruppe);
                }

                // Alle Klassenleitungen bilden eine Verterteilergruppe

                Verteilergruppe klassenlehrerVerteilergruppe = new Verteilergruppe();

                klassenlehrerVerteilergruppe.Name = "Klassenleitungen";

                foreach (var klasse in klasses)
                {   
                    
                    // Alle Lehrer, die in dieser Klasse unterrichten werden der VG hinzugefügt.

                    klassenlehrerVerteilergruppe.Lehrers.AddRange(klasse.Klassenleitungen);
                }

                // Duplikate filtern, Nullwerte filtern

                klassenlehrerVerteilergruppe.Lehrers = (from k in klassenlehrerVerteilergruppe.Lehrers select k).Where(x => x != null).Distinct().ToList();

                this.Add(klassenlehrerVerteilergruppe);

                // Das Kollegium bildet eine Verteilergruppe

                Verteilergruppe kollegium = new Verteilergruppe();
                kollegium.Name = "Kollegium";

                // Alle Lehrer, die in dieser Klasse unterrichten werden der VG hinzugefügt.

                kollegium.Lehrers = new List<Lehrer>();

                kollegium.Lehrers.AddRange(lehrers);
                this.Add(kollegium);

                Verteilergruppe unterrichtende = new Verteilergruppe();
                unterrichtende.Name = "Unterrichtende";

                // Alle Lehrer, die in dieser Klasse unterrichten werden der VG hinzugefügt.

                unterrichtende.Lehrers = new List<Lehrer>();

                unterrichtende.Lehrers.AddRange((from u in unterrichts
                                            select (from l in lehrers
                                                    where l.Kürzel == u.LehrerKürzel
                                                    select l).FirstOrDefault()).ToList().Distinct());
                this.Add(unterrichtende);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }

        public Verteilergruppes()
        {
            try
            {
                string strResult = string.Empty;
                RunspaceConfiguration rsConfig = RunspaceConfiguration.Create();
                PSSnapInException snapInException = null;
                PSSnapInInfo info = rsConfig.AddPSSnapIn("Microsoft.Exchange.Management.PowerShell.Snapin", out snapInException);
                Runspace runspace = RunspaceFactory.CreateRunspace(rsConfig);
                runspace.Open();

                Command getDistributionGroup = new Command("Get-DistributionGroup");

                Pipeline commandPipeLine = runspace.CreatePipeline();
                commandPipeLine.Commands.Add(getDistributionGroup);

                Collection<PSObject> exchangeDistributionGroups = commandPipeLine.Invoke();

                if (exchangeDistributionGroups.Count > 0)
                {
                    foreach (PSObject exchangeDistributionGroup in exchangeDistributionGroups)
                    {
                        Lehrers lehrers = new Lehrers();

                        Command getDistributionGroupMember = new Command("Get-DistributionGroupMember");

                        commandPipeLine = runspace.CreatePipeline();
                        commandPipeLine.Commands.Add(getDistributionGroupMember);
                        getDistributionGroupMember.Parameters.Add(new CommandParameter("Identity", exchangeDistributionGroup.Properties["Name"].Value.ToString()));

                        Collection<PSObject> resultsPersonen = commandPipeLine.Invoke();

                        // Für jeden bereits im Exchange existierenden Member wird geprüft, ...

                        if (resultsPersonen.Count > 0)
                        {
                            foreach (PSObject resultPerson in resultsPersonen)
                            {
                                // ... ob der Member noch immer Member der Exchange-Verteilergruppe ist.

                                var lehrer = (from l in lehrers
                                                  // Bei Lehrern matcht die ID, bei Schülern der username
                                              where l.Mail.Replace("@berufskolleg-borken.de", "").ToLower() == resultPerson.Properties["Alias"].Value.ToString().ToLower()
                                              select l).FirstOrDefault();
                                lehrers.Add(lehrer);
                            }
                        }
                        this.Add(new Verteilergruppe(exchangeDistributionGroup.Properties["Name"].Value.ToString(), lehrers));
                    }
                    Console.WriteLine("[i] " + this.Count + " bestehende Verteilergruppen aus Exchange eingelesen.");
                    Console.ReadKey();
                }
                runspace.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}