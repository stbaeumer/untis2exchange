﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Untis2Exchange
{
    public class Pausenaufsichts : List<Pausenaufsicht>
    {
        public Pausenaufsichts()
        {
        }

        public Pausenaufsichts(string aktSj, int aktPeriode, DateTime datumMontagDerKalenderwoche, Lehrers lehrers, Feriens feriens, string connectionString)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT
Corridor.CORRIDOR_ID, 
Corridor.Name, 
Corridor.BreakSupervision1,
Corridor.BreakSupervision2,
Corridor.BreakSupervision3,
Corridor.BreakSupervision4,
Corridor.BreakSupervision5,
Corridor.BreakSupervision6,
Corridor.BreakSupervision7,
Corridor.BreakSupervision8,
Corridor.BreakSupervision9,
Corridor.BreakSupervision10,
Corridor.BreakSupervision11,
Corridor.BreakSupervision12
FROM Corridor
WHERE(((Corridor.SCHOOLYEAR_ID)= " + aktSj + ") AND((Corridor.Deleted)= False))";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        string name = Global.SafeGetString(oleDbDataReader, 1);
                        string gesamt = Global.SafeGetString(oleDbDataReader, 2);
                        int periode = 0;
                        string lehrerKürzel = "";
                        int dauer = 0;
                        int tag = 0;                        
                        int stunde = 0;

                        for (int r = 2; r <= 13; r++)
                        {
                            foreach (var item in Global.SafeGetString(oleDbDataReader, r).Split(','))
                            {
                                if (item != "")
                                {
                                    try
                                    {
                                        periode = Convert.ToInt32(item.Split('~')[0]);
                                        lehrerKürzel = (from l in lehrers where l.IdUntis == Convert.ToInt32(item.Split('~')[1]) select l.Kürzel).FirstOrDefault();
                                        tag = Convert.ToInt32(item.Split('~')[2]);         
                                        dauer = Convert.ToInt32(item.Split('~')[4]);         
                                        stunde = Convert.ToInt32(item.Split('~')[3]);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex.ToString());
                                        throw new Exception(ex.ToString());
                                    }

                                    Pausenaufsicht pausenaufsicht = new Pausenaufsicht(oleDbDataReader.GetInt32(0), periode, lehrerKürzel, name, dauer, "", tag, stunde, datumMontagDerKalenderwoche);
                                    
                                    if (periode == aktPeriode)                                    
                                    {
                                        this.Add(pausenaufsicht);                                       
                                    }
                                }
                            }
                        }
                    };

                    Console.WriteLine(("Pausenaufsichten " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        public Pausenaufsichts AusfälleHerausfiltern(Lehrer lehrer, Feriens feriens, Substitutions substitutions, DateTime datumTagDerKalenderwoche)
        {
            try
            {
                Pausenaufsichts pausenaufsichtsDiesesLehrers = new Pausenaufsichts();

                foreach (var pausenaufsicht in (from p in this
                                                where p.LehrerKürzel == lehrer.Kürzel
                                                where p.Beginn.Date == datumTagDerKalenderwoche.Date
                                                select p).ToList())
                {
                    if (pausenaufsicht.LehrerKürzel == "OST" && pausenaufsicht.Beginn.Date.Month == 5)
                    {
                        string a = "";
                    }
                    bool fälltAusWegenFerien = false;

                    foreach (var ferien in feriens)
                    {
                        // Wenn die Pausenaufsicht nach Ferienbeginn ...
                        if (ferien.Von.Date <= pausenaufsicht.Beginn.Date)
                        {
                            // ... und vor dem Ende derselben Ferien liegt, ...
                            if (pausenaufsicht.Beginn.Date <= ferien.Bis.Date)
                            {
                                // ... dann findet die Pausenaufsicht jicht statt.
                                fälltAusWegenFerien = true;
                            }
                        }                        
                    }

                    bool fälltAusWegenVertretungsregelung = false;

                    foreach (var substitution in substitutions)
                    {
                        if (substitution.CorridorName != "")
                        {
                            if (substitution.Datum == datumTagDerKalenderwoche)
                            {
                                if (substitution.Stunde == pausenaufsicht.Stunde)
                                {
                                    if (substitution.BreakSubst != null)
                                    {
                                        if (substitution.ZuVertretender != null)
                                        {
                                            if (substitution.ZuVertretender.Kürzel == pausenaufsicht.LehrerKürzel)
                                            {
                                                fälltAusWegenVertretungsregelung = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }                        
                    }

                    if (fälltAusWegenVertretungsregelung || fälltAusWegenFerien)
                    {
                        string anmerkung = "(" + (fälltAusWegenVertretungsregelung ? "wg. Vertretung;" : "") + (fälltAusWegenFerien ? "wg. Ferien" : "") + ")";

                        Console.WriteLine(" [-] " + lehrer.Kürzel.PadRight(4) + datumTagDerKalenderwoche.ToShortDateString() + " " + datumTagDerKalenderwoche.DayOfWeek.ToString().PadRight(15) + " " + pausenaufsicht.Beginn + "-" + pausenaufsicht.Ende.ToShortTimeString());
                    }
                    else
                    {
                        pausenaufsichtsDiesesLehrers.Add(pausenaufsicht);
                        Console.WriteLine(" [+] " + lehrer.Kürzel.PadRight(4) + datumTagDerKalenderwoche.ToShortDateString() + " " + datumTagDerKalenderwoche.DayOfWeek.ToString().PadRight(15) + " " + pausenaufsicht.Beginn + "-" + pausenaufsicht.Ende.ToShortTimeString());
                    }
                }
                return pausenaufsichtsDiesesLehrers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler bei Lehrer " + lehrer.Kürzel + "\n" + ex.ToString());
                throw new Exception("Fehler bei Lehrer " + lehrer.Kürzel + "\n" + ex.ToString());
            }            
        }        
    }
}
